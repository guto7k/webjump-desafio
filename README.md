# Desafio Webjump - Frontend
Uma aplicação desenvolvida em HTML5, PHP, CSS3, JS, SASS, BEM, Gulp

## Live test 
- Foi criada uma VM com ambiente LAMP no Google Cloud Console para rodar a aplicação ([link](https://wemize.io))

## Requisitos
- Web Service PHP (Apache2 ou Nginx)
- NPM v6.14.4+ e Node 10.19.0+

## Como rodar a aplicação
Instale o NPM
```
npm install
```

Instale o gulp-cli
```
npm install --global gulp-cli
```

Instale o gulp caso necessário
```
npm install gulp -D
```

Rodar a aplicação local
```
gulp
```
- Conferir o proxy usado na task ```watcher``` se está com o path correto, no arquivo ```gulpfile.js```, caso necessário


Executar as tasks para distribuição
```
gulp build
```

- Para abrir a aplicação pronta para distribuição, acesse ```http://localhost:3000/webjump-desafio/dist/``` ou altere apenas o caminho da url de produção de **/public/** para **/dist/**

## Otimizações em geral
- As tags ```<head>```, ```<header>``` e ```<footer>``` são incluidas nas páginas pela função include(), do PHP
- Design responsivo com breakpoints (320px, 768px, 1024px e 1440px)
- SASS e CSS estruturados em padrão BEM (Block Element Modifier)
- Código semântico
- SEO implantado
- Meta Tags, Open Graph e Structured Data
- ```sitemap.xml``` e ```robots.txt```
- Performance e otimização

## Otimizações realizadas pelo Gulp para o ambiente de produção
- Auto-reload do Browser a partir de qualquer alteração feita em qualquer arquivo
- Auto-compile do SASS a partir de qualquer alteração feita em arquivos ```.scss```

## Otimizações realizadas pelo Gulp para o ambiente de distribuição
- Path's devidamente alterados de ```/public/``` para ```/dist/```, para rodar local
- Arquivos ```.html```, ```.php```, ```.css``` e ```.js``` minificados
- Comentários removidos
- Imagens otimizadas
- Arquivos ```.css``` comprimidos em apenas um e injetado junto ao final do ```head```
- Arquivos ```.js``` comprimidos em apenas um e injetado junto ao final do ```footer```
- Toda vez que um arquivo ```.css``` ou ```.js``` for alterado, a task de inject vai renomear o arquivo injetado, junto ao nome de arquivo chamado pelo ```<head>``` e ```<footer>```, para forçar o download caso houver cache

## App preview
![alt text](https://bitbucket.org/guto7k/webjump-desafio/raw/5c1d1b4c50ecde17718a8bac1ec8b5ac6a2ed679/assets/app-preview.png "App Preview - Webjump Desafio")

## Lighthouse
![alt text](https://bitbucket.org/guto7k/webjump-desafio/raw/da75841f3cc89f83539f85edf1a5fad794b442f8/assets/lighthouse-webjump-desafio.png "Lighthouse Result- Webjump Desafio")