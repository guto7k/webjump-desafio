<!DOCTYPE html>
<html lang="pt-br">

<?php

// LOCAL VAR
$title = 'Webjump - Camisetas';
$description = 'Aqui você encontra as melhores camisetas do mercado, só na Webjump!';
$openGraphDescription = 'Aqui você encontra as melhores camisetas do mercado, só na Webjump!';;
$openGraphImage = 'https://webjump.com.br/wp-content/uploads/2018/11/IMG_3330.png';;

?>

<!-- META++ -->
<?php include('assets/include/head.php'); ?>

<body>
	<!-- HEADER -->
	<?php include('assets/include/header.php'); ?>

	<!-- MAIN -->
	<main class="main">
		<div class="container">
			<div class="row">
				<div class="container-fluid">
					<div class="product-breadcrumb">
						<ul class="product-breadcrumb-menu">
							<li class="product-breadcrumb-menu__item">
								<a class="product-breadcrumb-menu__link" href='/'>Página Inicial&nbsp;</a>
							</li>
							<span class="product-breadcrumb-menu__span">>&nbsp;</span>
							<li class="product-breadcrumb-menu__item">
								<a class="product-breadcrumb-menu__link product-breadcrumb-menu__link--product" href="camisetas">Camisetas</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-lg-3">
					<?php include('assets/include/product-filter.php') ?>
				</div>

				<div class="col-12 col-lg-9">
					<h1 class="product__title">camisetas</h1>

					<?php include('assets/include/product-midbar.php') ?>

					<div class="row product-grid">
						<!-- api consume -->
					</div>

					<button class="product__btn-more" onclick="moreProduct(1)">mais produtos</button>
				</div>
			</div>
		</div>


	</main>

	<!-- FOOTER -->
	<?php include('assets/include/footer.php'); ?>

	<script> getGridButtons(); getProduct(1); </script>
</body>
</html>