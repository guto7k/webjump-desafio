User-agent: *
Allow: /
Disallow: /.git/
Disallow: /assets/include/states/

Sitemap: https://wemize.io/sitemap.xml