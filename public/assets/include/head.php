<head>
	<?php include('assets/include/head-global.php'); ?>

	<title><?php echo $title ?></title>

	<!-- META DEFAULT -->
	<meta charset="utf-8">
	<meta name="robots" content="index, follow">
	<meta name="language" content="pt-br">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="theme-color" content="#cb0d1f">
	<meta name="msapplication-TileColor" content="#cb0d1f">
	<meta name="msapplication-TileImage" content="assets/img/favicon/favicon-270x270.png" />

	<!-- META SEO -->
	<meta name="description" content="<?php echo $description ?>">
	<meta name="author" content="<?php echo AUTHOR ?>">
	<meta name="copyright" content="<?php echo COMPANY ?>">

	<!-- TWITTER CARDS -->
	<meta property="twitter:card" content="summary_large_image">
	<meta property="twitter:creator" content="@<?php echo TCUSER ?>">
	<meta property="twitter:site" content="@<?php echo TCUSER ?>">
	<meta property="twitter:title" content="<?php echo $title ?>">
	<meta property="twitter:description" content="<?php echo $openGraphDescription ?>">
	<meta property="twitter:image:src" content="<?php echo $openGraphImage ?>">

	<!-- OPEN GRAPH -->
	<meta property="og:title" content="<?php echo $title ?>">
	<meta property="og:description"content="<?php echo $openGraphDescription ?>">
	<meta property="og:image" content="<?php echo $openGraphImage ?>">
	<meta property="og:image:width" content="552">
	<meta property="og:image:height" content="339">
	<meta property="og:url" content="<?php echo URL ?>">
	<meta property="og:type" content="website">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:region" content="Brasil">
	<meta property="fb:app_id" content="<?php echo FBAPPID ?>">

	<!-- FAVICON -->
	<link rel="icon" href="assets/img/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" href="assets/img/favicon/favicon-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="assets/img/favicon/favicon-180x180.png" />
	
	<!-- REL -->
	<link rel="canonical" href="<?php echo URL ?>">
	<!-- inject:css -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- endinject -->


	<!-- STRUCTURED DATA -->
	<script type="application/ld+json">
		{ "@context": "https://schema.org",
		"@type": "Organization",
		"name": "Webjump",
		"legalName" : "Webjump",
		"url": "https://webjump.com.br/",
		"logo": "https://webjump.com.br/wp-content/uploads/2018/09/logo_webjump.png",
		"foundingDate": "2007",
		"founders": [
		{
			"@type": "Person",
			"name": "Erick Melo"
		},
		{
			"@type": "Person",
			"name": "Ivan Bastos"
		} ],
		"address": {
		"@type": "PostalAddress",
		"streetAddress": "Av. Pres. Juscelino Kubitschek, 50",
		"addressLocality": "Vila Nova Conceição",
		"addressRegion": "SP",
		"postalCode": "04543-000",
		"addressCountry": "BR"
	},
	"contactPoint": {
	"@type": "ContactPoint",
	"contactType": "customer support",
	"telephone": "[+11-2338-6889]",
	"email": "contato@webjump.com.br"
},
"sameAs": [ 
"https://www.facebook.com/webjump",
"https://www.linkedin.com/company/webjump/?",
"https://twitter.com/_webjump",
"https://www.instagram.com/webjump",
]}
</script>
</head>