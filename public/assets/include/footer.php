<footer class="footer">
	<div class="container-fluid">
		<div class="footer-bar"></div>
	</div>
</footer>

<!-- inject:js -->
<script src="assets/js/menu.js"></script>
<script src="assets/js/product.js"></script>
<script src="assets/js/main.js"></script>
<!-- endinject -->