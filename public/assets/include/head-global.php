<?php

// META SEO
define(COMPANY, 'Webjump');
define(AUTHOR, 'Webjump');

// TWITTER CARD
define(TCUSER, '62598471');

// FBAPPID
define(FBAPPID, 'webjump');

// URL
define(URL, (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
	"https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

?>