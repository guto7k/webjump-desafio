<header class="header">
	<div class="header-topbar">
		<div class="container-fluid">
			<div class="topbar">					
				<p class="topbar__option">
					<a class="topbar__access" href="#">Acesse sua conta</a> ou <a class="topbar__access header-topbar__access" href="#">cadastre-se</a>
				</p>
			</div>
		</div>
	</div>

	<div class="header-midbar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="row">
						<div class="col-2 col-sm-3 midbar-mobile">
							<a class="midbar-mobile-menu" href="#">
								<i class="fa fa-bars"></i>
							</a>
						</div>

						<div class="col-8 col-sm-6 col-md-4">
							<a class="midbar-logo" href="http://localhost:3000/webjump-desafio/prod/">
								<img class="midbar-logo__img" src="assets/img/webjump-logo.png" alt="Webjump - Logotipo">	
							</a>
						</div>

						<div class="col-2 col-sm-3 midbar-mobile">
							<button class="midbar-mobile-search">
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</div>

				<div class="col-12 col-md-8 midbar-search">
					<label class="midbar-search__label" for="midbar-search__input">Search: </label>
					<input id="midbar-search__input" class="midbar-search__input" type="text">
					<button class="midbar-search__button">buscar</button>
				</div>
			</div>
		</div>
	</div>

	<div class="header-bottombar">
		<div class="container-fluid">
			<nav class="bottombar-nav">
				<ul class="bottombar-menu">
					<!-- api consume -->
				</ul>
				<a href="#" class="bottombar-nav__close">x</a>
			</nav>
		</div>
	</div>
</header>