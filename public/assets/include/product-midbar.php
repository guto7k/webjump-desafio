<div class="product-midbar">
	<div class="row">
		<div class="col-5">
			<button class="product-midbar__btn-grid product-midbar__btn-grid--horizontal product-midbar__btn-grid--active">
				<i class="product-midbar__btn-grid-icon fa fa-grip-grid"></i>
			</button>
			<button class="product-midbar__btn-grid product-midbar__btn-grid--lines">
				<i class="product-midbar__btn-grid-icon fa fa-grip-no-grid"></i>
			</button>
		</div>

		<div class="col-7">
			<div class="product-midbar-order">
				<label class="product-midbar-order__label" for="product-midbar-order__select">ordenar por</label>

				<select class="product-midbar-order__select" id="product-midbar-order__select">
					<option value="preco">Preço</option>
					<option value="categoria">Categoria</option>
					<option value="cor">Cor</option>
				</select>
			</div>
		</div>
	</div>
</div>