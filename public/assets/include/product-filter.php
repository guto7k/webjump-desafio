<div class="product-filter">
	<nav class="product-filter-sidebar">
		<h1 class="product-filter__title">filtre por</h1>

		<h4 class="product-filter__subtitle">categorias</h4>
		<ul class="product-filter-list">
			<li class="product-filter-list__item">
				<a class="product-filter-list__link" href="#">roupas</a>
			</li>
			<li class="product-filter-list__item">
				<a class="product-filter-list__link" href="#">sapatos</a>
			</li>
			<li class="product-filter-list__item">
				<a class="product-filter-list__link" href="#">acessórios</a>
			</li>
		</ul>

		<h4 class="product-filter__subtitle">cores</h4>
		<ul class="product-filter-list product-filter-list--colors">
			<li class="product-filter-list__item product-filter-list__item--color">
				<a class="product-filter-list__color product-filter-list__color--red" href="#"></a>
			</li>
			<li class="product-filter-list__item product-filter-list__item--color">
				<a class="product-filter-list__color product-filter-list__color--orange" href="#"></a>
			</li>
			<li class="product-filter-list__item product-filter-list__item--color">
				<a class="product-filter-list__color product-filter-list__color--green" href="#"></a>
			</li>
		</ul>

		<h4 class="product-filter__subtitle">tipo</h4>
		<ul class="product-filter-list">
			<li class="product-filter-list__item">
				<a class="product-filter-list__link" href="#">corrida</a>
			</li>
			<li class="product-filter-list__item">
				<a class="product-filter-list__link" href="#">caminhada</a>
			</li>
			<li class="product-filter-list__item">
				<a class="product-filter-list__link" href="#">casual</a>
			</li>
			<li class="product-filter-list__item">
				<a class="product-filter-list__link" href="#">social</a>
			</li>
		</ul>
	</nav>
</div>