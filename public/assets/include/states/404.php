<?php

// META
$title = 'Referência em Magento na América Latina';
$description = 'Somos a Webjump, especializados em desenvolvimento de eCommerce totalmente personalizado, com total referência em Magento na América Latina.';
$openGraphDescription = 'Somos a Webjump, especializados em desenvolvimento de eCommerce totalmente personalizado, com total referência em Magento na América Latina.';
$openGraphImage = 'https://webjump.com.br/wp-content/uploads/2018/11/IMG_3330.png';

?>

<?php include('../head.php'); ?>

<!-- meta overwrite -->
<meta name="robots" content="noindex, nofollow">

<!-- footer style overwrite -->
<style>
	.footer {
		position: fixed;
		bottom: 0;
		width: 100%;
	}
</style>

<body>
	<!-- HEADER -->
	<?php include('../header.php'); ?>

	<!-- MAIN -->
	<main class="main">
		<div class="main-state">
			<h1 class="main-state__message">Not found, mate! :(</h1>
			<a class="main-state__button" href="/">Go home, stay home</a>
		</div>
	</main>

	<!-- FOOTER -->
	<?php include('../footer.php'); ?>
</body>
</html>