'use strict'

// consume menu api
const getMenu = () => {
	fetch('mock-api/V1/categories/list')
	.then(response => {
		if(!response.ok) throw new Error('Erro ao executar requisição (Status ' + response.status + ')')
			return response.json()
	})
	.then(menu => {
		let homePath
		if ( (window.location.hostname === 'localhost') || (window.location.hostname == '127.0.0.1') ){ 
			homePath = '/webjump-desafio/public/'
		} else {
			homePath = '/'
		}

		// first static menu items
		buildMenu('.bottombar-menu', 'bottombar-menu__item', 'Página Inicial', homePath, 'bottombar-menu__link')
		if ( (window.location.pathname == '/') || ((window.location.pathname == '/webjump-desafio/public/')) ) {
			buildMenu('.main-sidenav-menu', 'main-sidenav-menu__item', 'Página Inicial', '', 'main-sidenav-menu__link')
		}

		// dinamic menu items
		menu.items.forEach(menu => {
			buildMenu('.bottombar-menu', 'bottombar-menu__item', menu.name, menu.path, 'bottombar-menu__link')
			if ( (window.location.pathname == '/') || ((window.location.pathname == '/webjump-desafio/public/')) ) {
				buildMenu('.main-sidenav-menu', 'main-sidenav-menu__item', menu.name, menu.path, 'main-sidenav-menu__link')
			}
		})

		// last static menu items
		buildMenu('.bottombar-menu', 'bottombar-menu__item', 'Contato', '#', 'bottombar-menu__link')
		if ( (window.location.pathname == '/') || ((window.location.pathname == '/webjump-desafio/public/')) ) { 
			buildMenu('.main-sidenav-menu', 'main-sidenav-menu__item', 'Contato', '#', 'main-sidenav-menu__link')
		}
	})
	.catch(error => {
		console.error(error.message)
	})
}

// dom menu
const buildMenu = (menuClass, itemClass, linkName, linkPath, linkClass) => {
	let elMenu = document.querySelector(menuClass)
	let elItem = document.createElement('li')
	let elItemAnchor = document.createElement('a')
	let elItemText = document.createTextNode(linkName)

	elItemAnchor.appendChild(elItemText)
	elItemAnchor.setAttribute('href', linkPath)
	elItemAnchor.classList.add(linkClass)
	elItem.classList.add(itemClass)
	elItem.appendChild(elItemAnchor)
	elMenu.appendChild(elItem);
}


getMenu()