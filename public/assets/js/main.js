'use strict'

const createMobileMenuButton = () => {
	// open mobile menu
	let buttonMobileMenu = document.querySelector('.midbar-mobile-menu')
	let closeMobileMenu = document.querySelector('.bottombar-nav__close')
	let mobileMenu = document.querySelector('.bottombar-nav')

	buttonMobileMenu.addEventListener('click', () => {
		mobileMenu.classList.toggle('bottombar-nav--open')
		closeMobileMenu.style.display = 'block'
	})
	closeMobileMenu.addEventListener('click', () => {
		mobileMenu.classList.toggle('bottombar-nav--open')
		closeMobileMenu.style.display = 'none'
	})

	// open mobile search
	let buttonMobileSearch = document.querySelector('.midbar-mobile-search')
	let mobileSearch = document.querySelector('.midbar-search')

	buttonMobileSearch.addEventListener('click', () => {
		mobileSearch.classList.toggle('midbar-search--open')
	})
}

// init
createMobileMenuButton()