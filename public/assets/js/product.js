'use strict'

// CONSUME PRODUCTS API
const getProduct = (category) => {
	fetch('mock-api/V1/categories/' + category)
	.then(response => {
		if(!response.ok) throw new Error('Erro ao executar requisição (Status ' + response.status + ')')
			return response.json()
	})
	.then(product => {
		product.items.forEach(product => {
			buildProduct(product.name, product.price, product.image)
		})
	})
	.catch(error => {
		console.error(error.message)
	})
}

// DOM PRODUCT
const buildProduct = (name, price, image) => {
	let elProductGrid = document.querySelector('.product-grid')
	let elDiv = document.createElement('div')
	let elFigure = document.createElement('figure')
	let elImg = document.createElement('img')
	let elFigCaption = document.createElement('figcaption')
	let elSpan = document.createElement('span')
	let elButton = document.createElement('button')

	elDiv.classList.add('col-6', 'col-sm-4', 'col-md-3', 'col-toggle')
	elImg.classList.add('product__img')

	elImg.src = image
	elImg.alt = name + ' - Webjump'
	elImg.title = name + ' - Webjump'

	elFigCaption.classList.add('product__name')
	elFigCaption.innerHTML = name

	elSpan.classList.add('product__price')
	elSpan.innerHTML = 'R$' + price

	elButton.classList.add('product__btn-buy')
	elButton.innerHTML = 'comprar'

	elProductGrid.appendChild(elDiv)
	elDiv.appendChild(elFigure)
	elFigure.appendChild(elImg)
	elFigure.appendChild(elFigCaption)
	elDiv.appendChild(elSpan)
	elDiv.appendChild(elButton)
}

// MORE PRODUCTS BUTTON
const moreProduct = (category) => {
	let btnMoreProduct = document.querySelector('.product__btn-more')
	let categories = [1, 2, 3]
	let pos = categories.indexOf(category)

	categories.splice(pos, 1)

	categories.forEach(item => {
		getProduct(item)
	});

	btnMoreProduct.style.display = 'none'
}

// GRID LAYOUT BUTTONS
let getGridButtons = () => {
	let btnGridHorizontal = document.querySelector('.product-midbar__btn-grid--horizontal')
	let btnGridLines = document.querySelector('.product-midbar__btn-grid--lines')

	btnGridHorizontal.addEventListener('click', () => {
		let colToggle = document.getElementsByClassName('col-toggle')
		let imgProduct = document.getElementsByClassName('product__img')
		let btnProduct = document.getElementsByClassName('product__btn-buy')

		for(let i = 0; i < colToggle.length; i++) {
			colToggle[i].classList.remove('col-12')
			colToggle[i].classList.add('col-6')
			colToggle[i].classList.add('col-sm-4')
			colToggle[i].classList.add('col-md-3')
			imgProduct[i].style.width = '100%'
			imgProduct[i].style.display = 'block'
			imgProduct[i].style.margin = '0 auto'
			btnProduct[i].style.width = '100%'
		}

		btnGridHorizontal.classList.add('product-midbar__btn-grid--active')
		btnGridLines.classList.remove('product-midbar__btn-grid--active')
	})
	btnGridLines.addEventListener('click', () => {
		let colToggle = document.getElementsByClassName('col-toggle')
		let imgProduct = document.getElementsByClassName('product__img')
		let btnProduct = document.getElementsByClassName('product__btn-buy')

		for(let i = 0; i < colToggle.length; i++) {
			colToggle[i].classList.add('col-12')
			colToggle[i].classList.remove('col-6')
			colToggle[i].classList.remove('col-sm-4')
			colToggle[i].classList.remove('col-md-3')
			imgProduct[i].style.width = '25%'
			imgProduct[i].style.display = 'block'
			imgProduct[i].style.margin = '0 auto'
			btnProduct[i].style.width = '25%'
		}

		btnGridLines.classList.add('product-midbar__btn-grid--active')
		btnGridHorizontal.classList.remove('product-midbar__btn-grid--active')
	})
}