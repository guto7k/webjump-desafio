<!DOCTYPE html>
<html lang="pt-br">

<?php

// LOCAL VAR
$title = 'Referência em Magento na América Latina';
$description = 'Somos a Webjump, especializados em desenvolvimento de eCommerce totalmente personalizado, com total referência em Magento na América Latina.';
$openGraphDescription = 'Somos a Webjump, especializados em desenvolvimento de eCommerce totalmente personalizado, com total referência em Magento na América Latina.';
$openGraphImage = 'https://webjump.com.br/wp-content/uploads/2018/11/IMG_3330.png';

?>

<!-- META++ -->
<?php include('assets/include/head.php'); ?>

<body>
	<!-- HEADER -->
	<?php include('assets/include/header.php'); ?>

	<!-- MAIN -->
	<main class="main">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-md-3">
					<nav class="main-sidenav">
						<ul class="main-sidenav-menu">
							<!-- api consume -->
						</ul>
					</nav>
				</div>

				<div class="col-12 col-md-9">
					<div class="main-banner"></div>

					<article class="main-welcome">
						<h1 class="main-welcome__title">Seja bem-vindo!</h1>
						<p class="main-welcome__lorem">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab deleniti tenetur, similique reiciendis, ad aliquam unde mollitia sint voluptatem vel totam reprehenderit explicabo sunt nisi blanditiis doloribus ipsa incidunt consequuntur! Sequi quia corrupti rerum pariatur iure voluptatem labore saepe modi quos aperiam necessitatibus veniam nihil ab soluta nobis dolore perferendis eaque voluptate temporibus eos nemo voluptatum, praesentium. Repudiandae, quo, fugit. Quis nostrum voluptatem officiis numquam explicabo blanditiis aspernatur et veritatis. Quaerat sint aspernatur, neque repudiandae, voluptatum sit nemo saepe est error quas tempore possimus cumque ipsam consectetur, quidem numquam at.</p>
					</article>
				</div>
			</div>
		</div>
	</main>

	<!-- FOOTER -->
	<?php include('assets/include/footer.php'); ?>
</body>
</html>