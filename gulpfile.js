'use strict'

// PLUGINS
const {series, parallel, src, dest, watch} = require('gulp')
const browserSync = require('browser-sync')
const sass = require('gulp-sass')

const phpMinify = require('gulp-php-minify')
const htmlMin = require('gulp-htmlmin')
const cleanCss = require('gulp-clean-css')
const babelMinify = require('gulp-babel-minify')
const rev = require('gulp-rev')
const concat = require('gulp-concat')
const inject = require('gulp-inject')
const replace = require('gulp-replace')

const imagemin = require('gulp-imagemin')
const imageminPngquant = require('imagemin-pngquant')
const imageminMozjpeg = require('imagemin-mozjpeg')

const save = require('gulp-savefile');
const clean = require('gulp-clean')
const copy = require('gulp-copy')

/* ************ */
/* PUBLIC TASKS */
/* ************ */
const watcher = () => {
	browserSync.init({
		proxy: '127.0.0.1:80/webjump-desafio/public/'
	});

	watch('public/assets/scss/*.scss', sassCompiler)
	watch('public/**/*').on('change', browserSync.reload)
}
const sassCompiler = () => {
	return src('public/assets/scss/main.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(dest('public/assets/css/'))
}
exports.default = watcher

/* ********** */
/* DIST TASKS */
/* ********** */
const cleanAll = () => {
	return src('dist/', {read: false, allowEmpty: true })
	.pipe(clean())
}

const packCss = () => {
	return src('public/assets/css/*.css')
	.pipe(concat('all.css'))
	.pipe(cleanCss({compatibility: 'ie8'}))
	.pipe(rev())
	.pipe(dest('dist/assets/css/', { allowEmpty: true }))
	.pipe(rev.manifest({merge: true}))
	.pipe(dest('dist/assets/css/'))
}

const packJs = () => {
	return src('public/assets/js/*.js')
	.pipe(concat('all.js'))
	.pipe(babelMinify())
	.pipe(rev())
	.pipe(dest('dist/assets/js/', { allowEmpty: true }))
	.pipe(rev.manifest({merge: true}))
	.pipe(dest('dist/assets/js/'))
}

const phpMinifier = () => {
	return src('public/**/*.php')
	.pipe(phpMinify())
	.pipe(dest('dist/', { allowEmpty: true }))
}

const imgMinifier = () => {
	return src(['public/assets/img/**/*.{jpg,png}'])
	.pipe(imagemin([
		imageminPngquant({
			quality: [0.80, 0.80]
		}),
		imageminMozjpeg({
			quality: 80
		}),
		], {verbose: true}))
	.pipe(dest('dist/assets/img/'));
}

const injectCss = () => {
	return src('dist/assets/include/head.php')
	.pipe(inject(src('dist/assets/css/*.css',
		{read: false}), {ignorePath: 'dist', addRootSlash: false}))
	.pipe(dest('dist/assets/include/'))
	.pipe(save())
}

const injectJs = () => {
	return src('dist/assets/include/footer.php')
	.pipe(inject(src('dist/assets/js/*.js',
		{read: false}), {ignorePath: 'dist', addRootSlash: false}))
	.pipe(dest('dist/assets/include/'))
	.pipe(save())
}

const htmlMinifier = () => {
	return src('dist/**/*.php')
	.pipe(htmlMin({ collapseWhitespace: true,
		removeComments: true // ignore gulp inject comments because the inject is before this
	}))
	.pipe(dest('dist/', { allowEmpty: true }))
}

const replaceUrls = () => {
	return src('dist/**/*')
	.pipe(replace('public', 'dist'))
	.pipe(dest('dist/'))
}

const copyEssentials = () => {
	return src(['public/**/*.html', 'public/**/*.txt', 'public/**/*.xml', 
		'public/**/*.woff2', 'public/.htaccess'])
	.pipe(dest('dist/', { allowEmpty: true }))
}
const copyAPI = () => {
	return src('public/mock-api/**/*')
	.pipe(dest('dist/mock-api/'))
}
const copyMedia = () => {
	return src('public/media/**/*')
	.pipe(dest('dist/media/'))
}

exports.build = series(
	cleanAll,
	packCss,
	packJs,
	phpMinifier,
	imgMinifier,
	injectCss,
	injectJs,
	htmlMinifier,
	replaceUrls,
	copyEssentials,
	copyAPI,
	copyMedia
	)